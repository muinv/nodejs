const BASE_URL = "https://jsonplaceholder.typicode.com";

const fetchAPI = async (path) => {
  const response = await fetch(BASE_URL + path);
  const data = await response.json();
  return data;
};

(async () => {
  const [users, posts, comments] = await Promise.all([
    fetchAPI("/users"),
    fetchAPI("/posts"),
    fetchAPI("/comments"),
  ]);

  // 2. Get data from all users from API above. You will get a list of 10 users.
  console.log(users);

  // 3. Get all the posts and comments from the API. Map the data with the users array. The data format should be like this:
  const usersWithPostsAndComments = users.map((user) => ({
    id: user.id,
    name: user.name,
    username: user.username,
    email: user.email,
    comments: comments
      .filter((comment) => comment.postId === user.id)
      .map((comment) => ({
        id: comment.id,
        postId: comment.postId,
        name: comment.name,
        body: comment.body,
      })),
    posts: posts
      .filter((post) => post.userId === user.id)
      .map((post) => ({
        id: post.id,
        title: post.title,
        body: post.body,
      })),
  }));
  console.log(usersWithPostsAndComments);

  // 4. Filter only users with more than 3 comments.
  const usersWithMoreThanThreeComments = usersWithPostsAndComments.filter(
    (user) => user.comments.length > 3
  );
  console.log(usersWithMoreThanThreeComments);

  // 5. Reformat the data with the count of comments and posts
  const newUsersWithPostsAndComments = users.map((user) => ({
    id: user.id,
    name: user.name,
    username: user.username,
    email: user.email,
    commentsCount: comments.filter((comment) => comment.postId === user.id)
      .length,
    postsCount: posts.filter((post) => post.userId === user.id).length,
  }));
  console.log(newUsersWithPostsAndComments);

  // 6. Who is the user with the most comments/posts?
  const maxComments = Math.max(
    ...newUsersWithPostsAndComments.map((user) => user.commentsCount)
  );
  const maxPosts = Math.max(
    ...newUsersWithPostsAndComments.map((user) => user.postsCount)
  );
  const userWithTheMostCommentsOrPosts = newUsersWithPostsAndComments.filter(
    (user) => user.commentsCount === maxComments || user.postsCount === maxPosts
  );
  console.log(userWithTheMostCommentsOrPosts);

  // 7. Sort the list of users by the postsCount value descending?
  const sortedUsers = usersWithPostsAndComments.sort(
    (a, b) => b.postsCount - a.postsCount
  );
  console.log(sortedUsers);

  // 8. Get the post with ID of 1 via API request, at the same time get comments for post ID of 1 via another API request. Merge the post data with format:
  const [post, postComments] = await Promise.all([
    fetchAPI("/posts/1"),
    fetchAPI("/comments?postId=1"),
  ]);
  const postWithComments = {
    ...post,
    comments: postComments,
  };
  console.log(postWithComments);
})();
